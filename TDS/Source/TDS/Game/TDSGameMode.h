// �2021-2022 Roman Kravchenko, Krasnogorsk, Russia. All rights reseved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDSGameMode.generated.h"

UCLASS(minimalapi)
class ATDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDSGameMode();
};



